//scrool navbar top
$(window).scroll(function(){
    if ($(this).scrollTop()>300){
        $(".navbar").css("transition", "all .3s, transform .3s");
        $('nav').css("background", "#303B48");
        
    }else if($(this).scrollTop()<300){
        $(".navbar").removeClass("bg-light");
        $(".navbar").addClass("navbar-dark");
        $('nav').css("background", "none");
    }
});


//efeito deslizar entre sections
$(document).ready(function (){
    $("#portifolio-button").click(function (){
        $("html, boby").animate({
            scrollTop: $("#portifolio-section").offset().top
        }, 1500);
    });
    
    $("#portifolio-link").click(function (){
        $("html, boby").animate({
            scrollTop: $("#portifolio-section").offset().top
        }, 1500);
    });

    $("#sobre-link").click(function (){
        $("html, boby").animate({
            scrollTop: $("#sobre-section").offset().top
        }, 1500);
    });

    $("#contato-link").click(function (){
        $("html, boby").animate({
            scrollTop: $("#contato-section").offset().top
        }, 1500);
    });

    $("#home-link").click(function (){
        $("html, boby").animate({
            scrollTop: $("#home-section").offset().top
        }, 1500);
    });
});